package com.ardinal.kotlinweather

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.widget.TextView
import com.ardinal.kotlinweather.api.WeatherApi
import com.ardinal.kotlinweather.model.WeatherResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.math.BigDecimal
import java.math.RoundingMode
import android.widget.Toast
import android.content.Intent
import java.security.Permission
import java.security.Permissions


class MainActivity : AppCompatActivity() {

    private var location: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getLocation()
    }

    private fun createWeather(weather: WeatherResponse?) {
        val mLongitude = findViewById<TextView>(R.id.mText_longitude)
        val mLatitude = findViewById<TextView>(R.id.mText_latitude)
        val mWeatherID = findViewById<TextView>(R.id.mText_weatherID)
        val mWeatherMain = findViewById<TextView>(R.id.mText_weatherMain)
        val mWeatherDesc = findViewById<TextView>(R.id.mText_weatherDesc)
        val mWeatherIcon = findViewById<TextView>(R.id.mText_weatherIcon)
        val mBase = findViewById<TextView>(R.id.mText_base)
        val mMainTemp = findViewById<TextView>(R.id.mText_mainTemp)
        val mMainPressure = findViewById<TextView>(R.id.mText_mainPressure)
        val mMainHumidity = findViewById<TextView>(R.id.mText_mainHumidity)
        val mMainTempMin = findViewById<TextView>(R.id.mText_mainTempMin)
        val mMainTempMax = findViewById<TextView>(R.id.mText_mainTempMax)
        val mMainSeaLevel = findViewById<TextView>(R.id.mText_mainSeaLevel)
        val mMainGrandLevel = findViewById<TextView>(R.id.mText_mainGrandLevel)
        val mWindSpeed = findViewById<TextView>(R.id.mText_windSpeed)
        val mWindDeg = findViewById<TextView>(R.id.mText_windDeg)
        val mCloudsAll = findViewById<TextView>(R.id.mText_cloudsAll)
        val mDt = findViewById<TextView>(R.id.mText_dt)
        val mSysMessage = findViewById<TextView>(R.id.mText_sysMessage)
        val mSysCountry = findViewById<TextView>(R.id.mText_sysCountry)
        val mSysSunrise = findViewById<TextView>(R.id.mText_sysSunrise)
        val mSysSunset = findViewById<TextView>(R.id.mText_sysSunset)
        val mTimezone = findViewById<TextView>(R.id.mText_timezone)
        val mID = findViewById<TextView>(R.id.mText_id)
        val mName = findViewById<TextView>(R.id.mText_name)

        if (weather != null) {
            mLongitude.text = weather.coord?.lat.toString()
            mLatitude.text = weather.coord?.lon.toString()

            val weatherItems = weather.weather

            if (weatherItems != null) {
                for (i in weatherItems) {
                    mWeatherID.text = i?.id.toString()
                    mWeatherMain.text = i?.main
                    mWeatherDesc.text = i?.description
                    mWeatherIcon.text = i?.icon
                }
            }
            mBase.text = weather.base.toString()
            mMainTemp.text = weather.main?.temp.toString()
            mMainPressure.text = weather.main?.temp.toString()
            mMainHumidity.text = weather.main?.humidity.toString()
            mMainTempMin.text = weather.main?.tempMin.toString()
            mMainTempMax.text = weather.main?.tempMax.toString()
            mMainSeaLevel.text = weather.main?.seaLevel.toString()
            mMainGrandLevel.text = weather.main?.grndLevel.toString()
            mWindSpeed.text = weather.wind?.speed.toString()
            mWindDeg.text = weather.wind?.deg.toString()
            mCloudsAll.text = weather.clouds?.all.toString()
            mDt.text = weather.dt.toString()
            mSysMessage.text = weather.sys?.message.toString()
            mSysCountry.text = weather.sys?.country
            mSysSunrise.text = weather.sys?.sunrise.toString()
            mSysSunset.text = weather.sys?.sunset.toString()
            mTimezone.text = weather.timezone.toString()
            mID.text = weather.id.toString()
            mName.text = weather.name
        }
    }

    @SuppressLint("CheckResult")
    fun getWeatherData(lat: Double, lon: Double) {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder().addInterceptor(interceptor)

        val retrofit = Retrofit.Builder()
        retrofit.client(httpClient.build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.BASE_URL)

        val service = retrofit.build().create(WeatherApi::class.java)

        service.getCurrentWeatherData(
            latitude = lat.toString(),
            longitude =  lon.toString(),
            appid = "42a824af533bd68cf45ca4fac276dddd"
        ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe( {
                if (it.isSuccessful) {
                    createWeather(it.body())
                }
                else {

                }
            }, {
                error -> error.printStackTrace()
            })
    }

    private fun getFineLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), 11)
        }
    }

    private fun getLocation() {
        if (applicationContext.checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                5000,
                10f,
                object : LocationListener {
                    override fun onLocationChanged(location: Location?) {
                        val latitude = roundDouble(location?.latitude, 2)
                        val longitude = roundDouble(location?.longitude, 2)
                        initWeather(latitude, longitude)
                    }

                    private fun roundDouble(value: Double?, places: Int): Double {
                        return BigDecimal(value!!).setScale(places, RoundingMode.HALF_UP).toDouble()

                    }

                    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

                    }

                    override fun onProviderEnabled(provider: String?) {

                    }

                    override fun onProviderDisabled(provider: String?) {

                    }
                }
            )
        }
        else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 12)
            getLocation()
        }
    }

    fun initWeather(lat: Double, lon: Double) {
        getWeatherData(lat, lon)
    }
}

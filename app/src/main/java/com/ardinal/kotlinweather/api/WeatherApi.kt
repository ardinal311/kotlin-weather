package com.ardinal.kotlinweather.api

import com.ardinal.kotlinweather.model.WeatherResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("/data/2.5/weather")
    fun getCurrentWeatherData(
        @Query("lat") latitude: String,
        @Query("lon") longitude: String,
        @Query("appid") appid: String):
            Observable<Response<WeatherResponse>>
}